
<h2>Create GasStation</h2>

<form method="POST" action="{{ route('gasStations.store') }}">
    @csrf
    <div>
        <label for="address">Address:</label>
        <input type="text" name="address" id="address" required>
    </div>

    <div>
        <label for="fullname">Fullname:</label>
        <input type="text" name="fullname" id="fullname" required>
    </div>

    <div>
        <label for="fuel">Fuel:</label>
        <input type="text" name="fuel" id="fuel" required>
    </div>

    <div>
        <label for="fuelprice">Fuel Price:</label>
        <input type="text" name="fuelprice" id="fuelprice" required>
    </div>

    <div>
        <button type="submit">Create</button>
    </div>
</form>

<a href="{{ route('gasStations.index') }}">Back to List</a>
