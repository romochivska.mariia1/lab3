<h1>Edit Gas Station</h1>

<form action="{{ route('gasStations.update', $gasStations->id) }}" method="POST">
    @csrf
    @method('PUT')

    <label for="fullname">Fullname:</label>
    <input type="text" name="fullname" value="{{ $gasStations->fullname }}"><br>

    <label for="address">Address:</label>
    <input type="text" name="address" value="{{ $gasStations->address }}"><br>

    <label for="fuel">Fuel:</label>
    <input type="text" name="fuel" value="{{ $gasStations->fuel }}"><br>

    <label for="fuelprice">Fuel Price:</label>
    <input type="text" name="fuelprice" value="{{ $gasStations->fuelprice }}"><br>

    <button type="submit">Update</button>
</form>

<a href="{{ route('gasStations.index') }}">Back to List</a>
