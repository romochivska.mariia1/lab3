<?php

namespace App\Http\Controllers;

use App\Models\GasStation;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
class GasStationsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $gasStations = GasStation::all();
        return view('gasStations.index', compact('gasStations'));
    }

    public function create()
    {
        return view('gasStations.create');
    }

    public function store(Request $request)
    {
        GasStation::create($request->all());

        return redirect()->route('gasStations.index');
    }

    public function show(GasStation $gasStations )
    {
        return view('gasStations.show', compact('gasStations'));
    }

    public function edit(GasStation $gasStations )
    {
        return view('gasStations .edit', compact('gasStations'));
    }

    public function update(Request $request, GasStation $gasStations )
    {

        $request->validate([
            'fullname' => 'required',
            'address' => 'required',
            'fuel' => 'required',
            'fuelprice' => 'required',
        ]);

        // Оновлення даних
        $gasStations ->update([
            'fullname' => $request->input('fullname'),
            'address' => $request->input('address'),
            'fuel' => $request->input('fuel'),
            'fuelprice' => $request->input('fuelprice'),
        ]);

        return redirect()->route('gasStations.index');
    }



    public function destroy(GasStation $gasStations )
    {
        $gasStations ->delete();
        return redirect()->route('gasStations.index');
    }
}
