<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GasstationsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/gasStations', [GasstationsController::class, 'index'])->name('gasStations.index');
Route::get('/gasStations/create', [GasstationsController::class, 'create'])->name('gasStations.create');
Route::post('/gasStations', [GasstationsController::class, 'store'])->name('gasStations.store');
Route::get('/gasStations/{gasStations}', [GasstationsController::class, 'show'])->name('gasStations.show');
Route::get('/gasStations/{gasStations}/edit', [GasstationsController::class, 'edit'])->name('gasStations.edit');
Route::put('/gasStations/{gasStations}', [GasstationsController::class, 'update'])->name('gasStations.update');



Route::delete('/gasStations/{gasStations}', [GasstationsController::class, 'destroy'])->name('gasStations.destroy');
