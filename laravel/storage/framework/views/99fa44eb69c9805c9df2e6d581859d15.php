<h1>Edit Zapravka</h1>

<form action="<?php echo e(route('gasStations.update', $zapravkas->id)); ?>" method="POST">
    <?php echo csrf_field(); ?>
    <?php echo method_field('PUT'); ?>

    <label for="fullname">Fullname:</label>
    <input type="text" name="fullname" value="<?php echo e($zapravkas->fullname); ?>"><br>

    <label for="address">Address:</label>
    <input type="text" name="address" value="<?php echo e($zapravkas->address); ?>"><br>

    <label for="fuel">Fuel:</label>
    <input type="text" name="fuel" value="<?php echo e($zapravkas->fuel); ?>"><br>

    <label for="fuelprice">Fuel Price:</label>
    <input type="text" name="fuelprice" value="<?php echo e($zapravkas->fuelprice); ?>"><br>

    <button type="submit">Update</button>
</form>

<a href="<?php echo e(route('gasStations.index')); ?>">Back to List</a>
<?php /**PATH C:\xampp\htdocs\finLab3\laravel\resources\views/gasStations/edit.blade.php ENDPATH**/ ?>
